import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../models/employee';
import { Observable } from 'rxjs';

/**
 * Autor: Sebastian Londoño
 * Servicios que consumen los endpoints para crear, consultar, actualizar y elimiar empleados
 */

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  // Url base local para todos los servicios
  url: string = 'http://localhost:8080/employees/';

  constructor( private httpClient: HttpClient) { }

  /**
   * Obtener lista de todos los empleados
   * @returns empleados
   */
  public getListEmployees(): Observable<Employee[]> {
    return this.httpClient.get<Employee[]>(`${this.url}find_all`);
  }

  /**
   * Eliminar un empleado
   * @param id del empleado a eliminar
   */
  public deleteEmployee(id: number): Observable<any> {
    return this.httpClient.delete<any>(`${this.url}delete/${id}`)
  }

  /**
   * Actualizar un empleado
   * @param id
   * @param employee
   */
  public updateEmployee(id: number, employee: Employee) :Observable<any> {
    return this.httpClient.put<any>(`${this.url}update/${id}`,employee);
  }

  /**
   * Guardar un empleado
   * @param employee con los datos a guardar
   * @returns el empleado registrado
   */
  public saveEmployee(employee: Employee) :Observable<Employee> {
    return this.httpClient.post<Employee>(`${this.url}save`, employee);
  }

  /**
   * Servicio para consultar un solo empleado
   * @param id del empleado a consultar
   * @returns el empleado seleccionado si este existe
   */
  public getEmployeeByID(id: number): Observable<Employee> {
    return this.httpClient.get<Employee>(`${this.url}find/${id}`);
  }

}
