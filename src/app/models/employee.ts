/**
 * Interfaz empleado con los campos necesarios para crear o consultar.
 */
export interface Employee {

  id: number;
  firstLastName: string;
  secondLastName:string;
  fisrtName:string;
  otherNames:string;
  countryJob:string;
  documentType:string;
  documentNumber:string;
  email:string;
  ingressDate:Date;
  area:string;
  state:string
  createAt:Date;

}
