import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { lastValueFrom, map, Observable, Subscription } from 'rxjs';
import { DatePipe } from '@angular/common';
import Swal from 'sweetalert2';
import { EmployeesService } from 'src/app/services/employees.service';
import { Employee } from 'src/app/models/employee';
import { ActivatedRoute, Router } from '@angular/router';

/**
 * @autor Sebastian Londoño
 * Componente de registro y edicion de empleado
 */
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  // Form reactivo para la creacion y edición del empleado
  form: FormGroup | any;

  // Variable para controlar y presentar el dominio que llevara el email del empleado
  dominioEmail: string = '';

  // Fecha actual
  dateToday: string | null = '';

  // Observable y suscription para escuchar evento cuando se cambie el país de empleo
  selectCountryJobObservable: Observable<void> = new Observable<void>;
  selectCountryJobSuscription: Subscription = new Subscription;

  // Observable y suscriptions para escuchar evento cuando se ingresen datos del primer apellido y nombre,
  // esto para ir generando el email de manera automatica
  valueAutomaticEmailObservable: Observable<void> = new Observable<void>;
  valueAutomaticEmailSuscription: Subscription = new Subscription;
  valueAutomaticEmailSuscription2: Subscription = new Subscription;

  // Var de tipo empleado donde se almacenaran los datos de un empleado que se quiera editar
  employee: Employee | null = null;
  isUpdate: boolean = false;

  registerDate: FormControl = new FormControl('');

  constructor(private fb: FormBuilder, private datePipe: DatePipe,
    private serviceEmployee: EmployeesService, public activatedRoute: ActivatedRoute, private router: Router) { }

  async ngOnInit() {

    // Dar formato a fecha actual para la fecha y hora de registro del empleado
    this.dateToday = this.datePipe.transform(new Date(), 'yyyy-MM-dd hh:mm:ss');

    this.registerDate.setValue(this.dateToday);

    this.createForm();

    this.captureIdRoute(this.form);

    this.form.controls['email'].disable();
    this.registerDate.disable();

    // Se suscribe a los observables ppara activarlos
    this.activateObservablesForm();

  }

  // NgOnDestroy para finalizar la suscripción de los observables activos
  ngOnDestroy(): void {
    this.unsuscribeObservables();
  }

  // Capturar id de la ruta para saber si sera un registro o edición de un empleado
  async captureIdRoute(form: FormGroup) {
    this.activatedRoute.params.subscribe(async params => {

      // Si la ruta tiene un id diferente de null o new, significa que se va a editar un empleado
      if (params['id'] !== null && params['id'] !== 'new') {
        let idEmployee: number = parseInt(params['id']);

        await this.getEmployeeByForUpdate(idEmployee).then(() => {
          this.dataEmployeeUpdate(this.employee);
          this.isUpdate = true;
          this.validateAllFormControls(form);
        });

      }
    });
  }

  // Guardar datos del empleado a editar dado el caso
  async getEmployeeByForUpdate(id: number) {
    this.employee = await lastValueFrom(this.serviceEmployee.getEmployeeByID(id));
  }

  // Activar observables para estar escuchando eventos que se generen en el formulario
  activateObservablesForm() {

    // Cuando se selecciona un pais de empleo se generará el dominio del email segun la selección
    this.selectCountryJobObservable = this.form.get('countryJob').valueChanges.pipe(
      map((text: string) => this.validateDominioEmail(text))
    );
    this.selectCountryJobSuscription = this.selectCountryJobObservable.subscribe();

    // Para ir generando automaticamente el email del empleado con el nombre y apellido de este
    this.valueAutomaticEmailObservable = this.form.get('firstLastName').valueChanges.pipe(
      map(() => this.automaticGenerateEmailValue())
    );
    this.valueAutomaticEmailSuscription = this.valueAutomaticEmailObservable.subscribe();

    this.valueAutomaticEmailObservable = this.form.get('fisrtName').valueChanges.pipe(
      map(() => this.automaticGenerateEmailValue())
    );
    this.valueAutomaticEmailSuscription2 = this.valueAutomaticEmailObservable.subscribe();
  }

  // Se cargan los datos del empleado a editar en el form
  dataEmployeeUpdate(employee: Employee | null) {
    this.form.patchValue({
      firstLastName: employee?.firstLastName,
      secondLastName: employee?.secondLastName,
      fisrtName: employee?.fisrtName,
      otherNames: employee?.otherNames,
      countryJob: employee?.countryJob,
      documentType: employee?.documentType,
      documentNumber: employee?.documentNumber,
      ingressDate: employee?.ingressDate,
      area: employee?.area,
      state: employee?.state,
      createAt: this.dateToday,
    });

  }

  // Generación automatica del email
  automaticGenerateEmailValue() {
    let valueFirstName: string = this.form.get('fisrtName').value;
    let valueFirstLastName: string = this.form.get('firstLastName').value;

    this.form.get('email').setValue(`${valueFirstName.toLowerCase().replace(/ /g, "")}.${valueFirstLastName.toLowerCase().replace(/ /g, "")}@${this.dominioEmail}`);

  }

  // Validación del dominio del email del empleado
  validateDominioEmail(term: string) {
    if (term == 'Colombia') {
      this.dominioEmail = 'cidenet.com.co';
      this.automaticGenerateEmailValue();
    }
    else if (term == 'Estados Unidos') {
      this.dominioEmail = 'cidenet.com.us';
      this.automaticGenerateEmailValue();
    }
  }

  // Validaciones de los campos del form para avisar al usuario que campos son o no validos
  campoNoValido(campo: string) {
    return this.form.get(campo).invalid && this.form.get(campo).touched;
  }

  campoValido(campo: string) {
    return this.form.get(campo).valid && this.form.get(campo).touched;
  }

  // Creacion y build del formulario con todas sus validaciones correspondientes
  createForm() {

    /**
     * Validaciones de apellidos y nombres con expresiones regulares
     */

    this.form = this.fb.group({
      firstLastName: ['', [Validators.required, Validators.pattern(/^[A-Z\s]+$/u), Validators.maxLength(20)]],
      secondLastName: ['', [Validators.required, Validators.pattern(/^[A-Z\s][A-Z\s]+$/u), Validators.maxLength(20)]],
      fisrtName: ['', [Validators.required, Validators.pattern(/^[A-Z\s][A-Z\s]+$/u), Validators.maxLength(20)]],
      otherNames: ['', [Validators.pattern(/^[A-Z\s][A-Z\s]+$/u), Validators.maxLength(50)]],
      countryJob: ['Seleccione País empleo', [Validators.required, this.validateSelectValue]],
      documentType: ['Seleccione Tipo Identificación', [Validators.required, this.validateSelectValue]],
      documentNumber: ['', [Validators.required, Validators.pattern(/^[a-zA-Z0-9-]+$/u), Validators.maxLength(20)]],
      email: [''],
      ingressDate: ['', [Validators.required, this.validateIngressDate]],
      area: ['Seleccione Área', [Validators.required, this.validateSelectValue]],
      state: { value: 'Activo', disabled: true },
    });

  }

  // Validacion de selects de area, pais empleo y tipo de identificacion por si llega un valor no permitido
  public validateSelectValue(control: AbstractControl) {
    const valueSelect = control.value;

    let error = null;
    if (valueSelect == 'Seleccione Área' || valueSelect == 'Seleccione País empleo' || valueSelect == 'Seleccione Tipo Identificación') {
      error = { valor: 'Valor invalido' };
    }
    return error;
  }

  // Validacion de fecha de ingreso
  public validateIngressDate(control: AbstractControl) {
    const valueDate = control.value;

    let datepipe: DatePipe = new DatePipe('en-US');

    let today: any = datepipe.transform(new Date(), 'yyy-MM-dd');
    let monthAgo: any = datepipe.transform(new Date(new Date().getFullYear(), new Date().getMonth() - 1, new Date().getDate()), 'yyy-MM-dd');

    let error = null;

    // Si la fecha selecciona es mayor a la actual
    if (valueDate > today) {
      error = { valor: 'Fecha de ingreso no puede ser mayor a la actual' };
    }

    // Solo puede ser menor hasta un mes
    else if (valueDate < monthAgo) {
      error = { valor: 'Fecha de ingreso solo puede ser hasta un mes menor' };
    }

    return error;

  }

  // Unsuscribe de los observables activos
  public unsuscribeObservables(): void {
    this.selectCountryJobSuscription.unsubscribe();
    this.valueAutomaticEmailSuscription.unsubscribe();
    this.valueAutomaticEmailSuscription2.unsubscribe();
  }

  // Registro del empleado
  public registerEmployee(form: FormGroup): void {

    // Validar si el formulario con los campos es valido
    if (form.invalid) {

      this.validateAllFormControls(form);

    }

    // Si el formulario es valido se procederá a guardar el empleado
    else {

      Swal.fire({
        title: 'Espere',
        text: 'Guardando información.....',
        icon: 'info',
        allowOutsideClick: false
      });

      Swal.showLoading();

      let originalEmail: string = this.form.get('email').value;

      // Se hace llamado del servicio para guardar el empleado y se pasa como parametro el form con los datos
      this.serviceEmployee.saveEmployee(this.form.getRawValue()).subscribe((resp) => {

        // Validación si ya existe un empleado con el mismo tipo y numero de documento
        if (resp == null) {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Ya existe un empleado con el mismo tipo y número de documento!',
          })
        }

        // Se guarda el empleado, se hace aviso de que el email generado ya existia y se informa del nuevo
        else if (originalEmail !== resp.email) {
          Swal.fire({
            title: `El empleado ${resp.fisrtName} ${resp.firstLastName}`,
            text: `Se agregó correctamente! Aviso: ${originalEmail} ya existe, el nuevo correo se guardó como: ${resp.email}`,
            icon: 'success'
          });

          this.unsuscribeObservables();
          this.router.navigate(['/home']);
        }

        else {
          Swal.fire({
            title: `El empleado ${resp.fisrtName} ${resp.firstLastName}`,
            text: `Se agregó correctamente!`,
            icon: 'success'
          });
          this.unsuscribeObservables();
          this.router.navigate(['/home']);
        }


      })

    }

  }

  // Actualizar empleado
  public updateEmployee(form: FormGroup): void {

    if (form.invalid) {
      this.validateAllFormControls(form);
    }

    else {

      Swal.fire({
        title: 'Espere',
        text: 'Actualizando información.....',
        icon: 'info',
        allowOutsideClick: false
      });

      Swal.showLoading();

      let originalEmail: string = this.form.get('email').value;

      this.serviceEmployee.updateEmployee(this.employee!.id, this.form.getRawValue()).subscribe((resp) => {

        if (resp == null) {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Ya existe un empleado con el mismo tipo y número de documento!',
          })
        }

        else if (originalEmail !== resp.email) {
          Swal.fire({
            title: `El empleado ${resp.fisrtName} ${resp.firstLastName}`,
            text: `Se actualizó correctamente! Aviso: ${originalEmail} ya existe, el nuevo correo se guardó como: ${resp.email}`,
            icon: 'success'
          });

          this.unsuscribeObservables();
          this.router.navigate(['/home']);
        }

        else {
          Swal.fire({
            title: `El empleado ${resp.fisrtName} ${resp.firstLastName}`,
            text: `Se actualizó correctamente!`,
            icon: 'success'
          });
          this.unsuscribeObservables();
          this.router.navigate(['/home']);
        }

      })
    }


  }

  // Validar automaticamente todos los campos del form
  validateAllFormControls(form: FormGroup) {
    (Object).values(form.controls).forEach(control => {

      //validacion si alguno de los controladores es un formgroup
      if (control instanceof FormGroup) {
        (Object).values(control.controls).forEach(control => control.markAsTouched());
      } else {
        control.markAsTouched();
      }

    });
  }

}
