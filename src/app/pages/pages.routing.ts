import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';

/**
 * Rutas para navegar por la aplicacion
 * ruta home: principal
 * ruta register/:id: registro o edicion de empleado
 */
const routes: Routes = [
  {
      path: 'home',
      component: PagesComponent,
      children: [
        {path: '', component: HomeComponent },
        {path: 'register/:id', component: RegisterComponent}
      ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
