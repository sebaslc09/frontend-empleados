import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { lastValueFrom, map, Observable, startWith } from 'rxjs';
import { Employee } from 'src/app/models/employee';
import { EmployeesService } from 'src/app/services/employees.service';
import Swal from 'sweetalert2';

/**
 * @autor Sebastian Londoño
 * Home principal donde estará la lista de los empleados junto con las opciones de edición y eliminación
 */

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // Variables para controlar paginación y empleados mostrados por pagina que en este caso son 10
  collectionEmployeesSize: number = 0;
  pageTable: number = 1;
  sizePages: number = 10;

  // Form control para campo de filtro
  searchFilter: FormControl = new FormControl('');

  // Diferentes listas de empleados para controlar filtros y validaciones
  employeesList: Employee[] = [];
  employeesListTable: Employee[] = [];
  employesListFilter: Employee[] = [];

  // Observable para escuchar evento en campo de filtro
  filterEmployeesObservable: Observable<Employee[]> = new Observable<Employee[]>;

  constructor(private employeeService: EmployeesService, private router: Router) { }

  async ngOnInit() {

    // Llamado de servicio para consultar todos los empleados
    await this.getAllEmployees().then(() => {
      this.employeesListTable = this.employeesList;
      this.collectionEmployeesSize = this.employeesList.length;
    });

    this.suscriptionObservableFilter();

  }

  //Suscription de observable para ir filtrando empleados segun el termino que se ingrese en el filtro
  public suscriptionObservableFilter() {
    this.filterEmployeesObservable = this.searchFilter.valueChanges.pipe(
      startWith(''),
      map(text => this.filterByAll(text))
    );
    this.filterEmployeesObservable.subscribe((employeesFiltered: Employee[]) => {
      this.employesListFilter = employeesFiltered;
      this.reloadEmployeesTable();
    });
  }

  /**
   * Acción de boton editar, se redirige a la pantalla de registro y edición, enviando como parametro el id del empleado a editar
   * @param id del empleado a editar
   */
  goUpdateEmployee(id: number) {
    this.router.navigate(['home/register/', id]);
  }

  // Llamado de servicio para consultar todos los empleados
  async getAllEmployees() {
    this.employeesList = await lastValueFrom(this.employeeService.getListEmployees());
  }

  // Reload que se llama cuando se ingresa algun filtro en la tabla
  reloadEmployeesTable() {
    this.employeesListTable = this.employesListFilter
      .slice((this.pageTable - 1) * this.sizePages, (this.pageTable - 1) * this.sizePages + this.sizePages);
  }

  /**
   * Eliminar un empleado
   * @param employee a eliminar
   */
  async deleteEmployee(employee: Employee) {

    // Alerta de confirmación
    Swal.fire({
      title: 'Está seguro de que desea eliminar el empleado?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      showClass: {
        popup: 'animate__animated animate__fadeInDown'
      },
      hideClass: {
        popup: 'animate__animated animate__fadeOutUp'
      }
    }).then((result) => {
      if (result.isConfirmed) {
        this.employeeService.deleteEmployee(employee.id).subscribe(async () => {

          // Se vuelve a consultar todos los empleados despues de eliminar al empleado
          await this.getAllEmployees().then(() => {
            this.employeesListTable = this.employeesList;
            this.collectionEmployeesSize = this.employeesList.length;
          });
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Se ha eliminado el empleado exitosamente!',
            showConfirmButton: false,
            hideClass: {
              popup: 'animate__animated animate__fadeOutUp'
            },
            timer: 1500
          })
        });
      }
    })

  }

  // Filtrado de empleados
  filterByAll(text: string): Employee[] {
    const term = text.toLowerCase();

    if (term == '' || term == null) {
      return this.employeesList;
    }
    else if (term !== '' && term !== null && this.employeesListTable.length <= 0) {
      this.employeesListTable = this.employeesList;
      return this.listEmployeesByFilter(term);
    }
    else {
      return this.listEmployeesByFilter(term);
    }

  }

  // Se retorna la lista de empleados con el filtro ingresado
  /**
   * Se filtra por: nombres, apellidos, tipo y numero de identificación, email, pais de empledo
   * @param term termino del filtro
   * @returns la lista de empleados
   */
  listEmployeesByFilter(term: string): Employee[] {

    return this.employeesListTable.filter(employe => {
      return employe.fisrtName.toString().toLowerCase().includes(term)
        || employe.otherNames.toLowerCase().includes(term)
        || employe.firstLastName.toLowerCase().includes(term)
        || employe.secondLastName.toLowerCase().includes(term)
        || employe.documentType.toLowerCase().includes(term)
        || employe.documentNumber.toLowerCase().includes(term)
        || employe.countryJob.toLowerCase().includes(term)
        || employe.email.toLowerCase().includes(term)
    });

  }

}
