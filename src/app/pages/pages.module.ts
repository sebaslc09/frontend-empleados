import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from '../app-routing.module';
import { PagesComponent } from './pages.component';
import { HomeComponent } from './home/home.component';
import { SharedModule } from '../shared/shared.module';
import { RegisterComponent } from './register/register.component';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [
    PagesComponent,
    HomeComponent,
    RegisterComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    PagesComponent,
    HomeComponent
  ],
  providers: [
    DatePipe
  ]
})
export class PagesModule { }
